FROM golang:1.21.3 as builder

ENV APP_HOME /go/src/golinks

WORKDIR "$APP_HOME"
COPY . .

RUN go mod download
RUN go mod verify
RUN go build -o golinks

EXPOSE 8080
CMD ["./golinks"]
