package db

import (
	"database/sql"
	"fmt"
	"golinks/model"
	"log"
	"time"
)

func (db *Database) AddLink(link *model.Link) error {
	log.Printf("adding link: %v", link)

	location, tsErr := time.LoadLocation("America/Chicago")

	if tsErr != nil {
		return tsErr
	}

	link.CreationTimestamp = time.Now().In(location).Format(time.RFC3339)

	_, getErr := db.GetLink(link.GoLink)

	if getErr == ErrNoMatch {
		query := `INSERT INTO golinks (go_link, redirect_url, creation_ts) VALUES ($1, $2, $3) RETURNING id`
		err := db.Conn.QueryRow(query, link.GoLink, link.RedirectURL, link.CreationTimestamp).Scan(&link.ID)
		if err != nil {
			log.Printf("err: %v", err)
			return err
		}
	} else if getErr != nil {
		return fmt.Errorf("An error occurred while checking for existing details for provided golink")
	}

	log.Println("Link already exists, returning")

	return nil
}

func (db *Database) GetLink(goLink string) (model.Link, error) {
	link := model.Link{}

	log.Printf("getting link: %s", goLink)

	query := `SELECT * FROM golinks WHERE go_link = $1`
	row := db.Conn.QueryRow(query, goLink)
	switch err := row.Scan(&link.ID, &link.GoLink, &link.RedirectURL, &link.CreationTimestamp); err {
	case sql.ErrNoRows:
		return link, ErrNoMatch
	default:
		return link, err
	}
}
