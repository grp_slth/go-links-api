package handler

import (
	"encoding/json"
	"golinks/db"
	"golinks/model"
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

var dbInstance db.Database

func NewHandler(db db.Database) http.Handler {
	router := chi.NewRouter()
	dbInstance = db

	router.Route("/", linkRouter)

	return router
}

func linkRouter(router chi.Router) {
	router.Get("/", sendHome)
	router.Put("/", saveLink)
	router.Route("/{link}", func(router chi.Router) {
		router.Get("/", getLink)
	})
}

// need to figure out how to route to UI
func sendHome(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://go.local.marcelosalinas.dev", 302)
}

func getLink(w http.ResponseWriter, r *http.Request) {
	link := chi.URLParam(r, "link")

	response, err := dbInstance.GetLink(link)

	log.Printf("response: %v", response)

	if err != nil {
		if err == db.ErrNoMatch {
			http.Redirect(w, r, "https://go.local.marcelosalinas.dev/create-link?not-found=true", 303)
		} else {
			http.Redirect(w, r, "https://go.local.marcelosalinas.dev/create-link?error=true", 303)
		}
	}

	http.Redirect(w, r, response.RedirectURL, 302)
}

func saveLink(w http.ResponseWriter, r *http.Request) {
	var link model.Link

	if err := json.NewDecoder(r.Body).Decode(&link); err != nil {
		response, _ := json.Marshal(BadRequestErrorBuilder(err))
		w.Write(response)
		return
	}

	if err := dbInstance.AddLink(&link); err != nil {

		w.Write([]byte("Failed to save link"))
	}
}
