package model

type Link struct {
	ID                string `json:"id"`
	GoLink            string `json:"goLink"`
	RedirectURL       string `json:"redirectURL"`
	CreationTimestamp string `json:"creationTimestamp"`
}
